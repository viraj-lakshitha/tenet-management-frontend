import { BrowserRouter } from "react-router-dom";
import { Switch, Route } from "react-router";
import HomeComponent from "./components/view/HomeComponent";
import ViewTenantComponent from "./components/view/ViewTenantComponent";
import UpdateTenantComponent from "./components/update/UpdateTenantComponent";
import UpdateDeviceComponent from "./components/update/UpdateDeviceComponent";
import AddDeviceComponent from "./components/create/AddDeviceComponent";
import AddTenantComponent from "./components/create/AddTenantComponent";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={HomeComponent} />
        <Route path="/view/tenants/:id" component={ViewTenantComponent} />
        <Route path="/add/tenants" component={AddTenantComponent} />
        <Route path="/add/devices/:id" component={AddDeviceComponent} />
        <Route path="/update/tenants/:id" component={UpdateTenantComponent} />
        <Route path="/update/devices/:id" component={UpdateDeviceComponent} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
