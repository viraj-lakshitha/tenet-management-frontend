import axios from "axios";

// GET Request
export const apiGetTenantData = () => {
  axios.get(`http://localhost:8080/api/v1/tenants`);
};
export const apiGetTenantDataById = (id) => {
  axios.get(`http://localhost:8080/api/v1/tenants/${id}`);
};
export const apiGetDeviceData = () => {
  axios.get(`http://localhost:8080/api/v1/tenants`);
};
export const apiGetDeviceDataById = (id) => {
  axios.get(`http://localhost:8080/api/v1/devices/${id}`);
};

// POST Request
export const apiPostTenantData = (data) => {
  axios.post(`http://localhost:8080/api/v1/tenants`, data);
};
export const apiPostDeviceData = (data) => {
  axios.post(`http://localhost:8080/api/v1/devices`, data);
};

// DELTE Request
export const apiDeleteTenantDataById = (id) => {
  axios.delete(`http://localhost:8080/api/v1/tenants/${id}`);
};
export const apiDeleteDeviceDataById = (id) => {
  axios.delete(`http://localhost:8080/api/v1/devices/${id}`);
};

// PUT Request
export const apiPutTenantData = (id, data) => {
  axios.post(`http://localhost:8080/api/v1/tenants/${id}`, data);
};
export const apiPutDeviceData = (id, data) => {
  axios.post(`http://localhost:8080/api/v1/devices/${id}`, data);
};
