import { useState, useEffect } from "react";
import CardComponent from "./CardComponent";
import HeaderComponent from "./HeaderComponent";
import "../../stylesheets/HomeComponent.css";
import AddNewCardComponent from "./AddNewCardComponent";
import axios from "axios";

const HomeComponent = () => {
  const [data, setData] = useState([]);

  const loadData = () => {
    axios.get(`http://localhost:8080/api/v1/tenants`).then((res) => {
      const tenantData = res.data;
      setData(tenantData);
      console.log(res.data);
    });
  };

  const deleteTenant = (id) => {
    if (window.confirm("Are you sure you want to delete ?")) {
      axios.delete(`http://localhost:8080/api/v1/tenants/${id}`).then((res) => {
        loadData();
      });
    }
  };

  useEffect(() => {
    loadData();
  }, []);

  return (
    <div>
      {loadData}
      <HeaderComponent />
      <div className="container">
        <div className="card-container">
          {data.map((tenant) => {
            return (
              <div key={tenant.id}>
                <CardComponent
                  id={tenant.id}
                  name={tenant.tenantName}
                  username={tenant.tenantUsername}
                  domain={tenant.tenantDomain}
                  address={tenant.tenantAddress}
                  status={tenant.tenantStatus}
                  deleteTenant={deleteTenant}
                />
              </div>
            );
          })}
          <AddNewCardComponent />
        </div>
      </div>
    </div>
  );
};

export default HomeComponent;
