import { IoIosAddCircleOutline } from "react-icons/io";
import { Link } from "react-router-dom";

import "../../stylesheets/AddNewCardComponent.css";

const AddNewCardComponent = () => {
  return (
    <Link className="link" to="/add/tenants">
      <div className="add-container-content">
        <h5>Add New Tenant</h5>
        <div>
          <IoIosAddCircleOutline size={25} />
        </div>
      </div>
    </Link>
  );
};

export default AddNewCardComponent;
