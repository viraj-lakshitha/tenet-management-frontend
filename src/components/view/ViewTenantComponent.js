import { FcBusinessman } from "react-icons/fc";
import { FiDelete, FiEdit } from "react-icons/fi";

import React, { useEffect, useState } from "react";
import HeaderComponent from "./HeaderComponent";
import "../../stylesheets/ViewTenantComponent.css";
import axios from "axios";
import { Table } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";

const ViewTenantComponent = () => {
  const [data, setData] = useState([]);
  const [devices, setDevices] = useState([]);
  const { id } = useParams();

  useEffect(() => {
    loadData();
  }, []);

  const loadData = () => {
    axios.get(`http://localhost:8080/api/v1/tenants/${id}`).then((res) => {
      setData(res.data);
      console.log(res.data);
      setDevices(res.data.tenantDevices);
      console.log(res.data.tenantDevices);
    });
  };

  return (
    <div>
      <HeaderComponent />
      <div className="view-container">
        <div className="tenant-details-container">
          <div className="tenant-details">
            <div className="view-avatar-container">
              <FcBusinessman size={200} />
            </div>
            <div className="tenant-details-table">
              <Table borderless={true}>
                <tbody>
                  <tr style={{ border: "1px solid #D5D8DC" }}>
                    <td style={{ borderRight: "1px solid #D5D8DC" }}>Name</td>
                    <td>{data.tenantName}</td>
                  </tr>
                  <tr style={{ border: "1px solid #D5D8DC" }}>
                    <td style={{ borderRight: "1px solid #D5D8DC" }}>
                      Description
                    </td>
                    <td>{data.tenantDescription}</td>
                  </tr>
                  <tr style={{ border: "1px solid #D5D8DC" }}>
                    <td style={{ borderRight: "1px solid #D5D8DC" }}>
                      Username
                    </td>
                    <td>{data.tenantUsername}</td>
                  </tr>
                  <tr style={{ border: "1px solid #D5D8DC" }}>
                    <td style={{ borderRight: "1px solid #D5D8DC" }}>Status</td>
                    <td>{data.tenantStatus ? "Active" : "Offline"}</td>
                  </tr>
                  <tr style={{ border: "1px solid #D5D8DC" }}>
                    <td style={{ borderRight: "1px solid #D5D8DC" }}>Domain</td>
                    <td>{data.tenantDomain}</td>
                  </tr>
                  <tr style={{ border: "1px solid #D5D8DC" }}>
                    <td style={{ borderRight: "1px solid #D5D8DC" }}>
                      Address
                    </td>
                    <td>{data.tenantAddress}</td>
                  </tr>
                </tbody>
              </Table>
            </div>
          </div>
          <div className="verticle-line">.</div>
        </div>

        <div className="device-details">
          <div className="button-group">
            <Link className="link" to={`/update/tenants/${id}`}>
              <button className="button-group-update">Update Tenant</button>
            </Link>

            <Link className="link" to={`/add/devices/${id}`}>
              <button className="button-group-add">Add New Device</button>
            </Link>
          </div>
          <div className="table-content">
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th className="device-table-header" colSpan="5">
                    Device List
                  </th>
                </tr>
                <tr>
                  <th>Device Name</th>
                  <th>Description</th>
                  <th>Serial Number</th>
                  <th>Status</th>
                  <th className="device-table-action">Action</th>
                </tr>
              </thead>
              <tbody>
                {devices.map((device) => {
                  return (
                    <tr>
                      <td>{device.deviceName}</td>
                      <td>{device.deviceDescription}</td>
                      <td>{device.deviceSerialNumber}</td>
                      <td>{device.deviceStatus ? "Active" : "Offline"}</td>
                      <td>
                        <Link className="link" to={`/update/devices/${device.id}`}>
                          <button style={{ backgroundColor: "transparent" }}>
                            <FiEdit size={20} />
                          </button>
                        </Link>
                        <button onClick={ () => {
                          axios.delete(`http://localhost:8080/api/v1/devices/${device.id}`).then((res) => {
                            console.log(res.data);
                            loadData();
                          });
                        }}>
                          <FiDelete size={20} />
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ViewTenantComponent;
