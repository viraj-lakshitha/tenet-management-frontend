import "../../stylesheets/HeaderComponent.css";
import { VscHome } from "react-icons/vsc";
import { Link } from "react-router-dom";

const HeaderComponent = () => {
  return (
    <div className="header-container">
      <div className="container-icon">
        <Link className="link" to="/">
          <VscHome size={30} />
        </Link>
      </div>
      <div className="container-header">
        <h3 className="header-text">Tenant Management</h3>
      </div>
    </div>
  );
};

export default HeaderComponent;
