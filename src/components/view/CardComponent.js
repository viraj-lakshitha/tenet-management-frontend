import { FcBusinessman } from "react-icons/fc";
import { GoPrimitiveDot } from "react-icons/go";
import { GrView } from "react-icons/gr";
import { RiDeleteBin5Line } from "react-icons/ri";
import { Link } from "react-router-dom";

import "../../stylesheets/CardComponent.css";

const CardComponent = ({
  id,
  name,
  username,
  domain,
  address,
  status,
  deleteTenant,
}) => {
  return (
    <div className="container-content">
      <div className="container-avatar">
        <div className="container-avatar-container">
          <FcBusinessman size={75} />
        </div>
      </div>

      <h5 className="card-user-name">
        {name} <GoPrimitiveDot color={status ? "green" : "red"} />
      </h5>
      <p>{username}</p>
      <p>{domain}</p>
      <p>{address}</p>

      <div className="container-button">
        <Link to={`/view/tenants/${id}`}>
          <button className="container-button-view">
            View <GrView />
          </button>
        </Link>

        <button
          onClick={() => {
            deleteTenant(id);
          }}
          className="container-button-delete"
        >
          Delete <RiDeleteBin5Line />
        </button>
      </div>
    </div>
  );
};

export default CardComponent;
