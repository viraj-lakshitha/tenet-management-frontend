import { useState, useEffect } from "react";
import "../../stylesheets/DeviceForm.css";
import { Form, Row, Col, Button } from "react-bootstrap";
import HeaderComponent from "../view/HeaderComponent";
import axios from "axios";
import { useParams } from "react-router-dom";

const initialState = {
  name: "",
  description: "",
  serial: "",
  status: false,
};

const UpdateDeviceComponent = () => {
  const { id } = useParams();
  const [data, setData] = useState([]);

  useEffect(() => {
    loadData();
  }, [id]);

  const loadData = () => {
    axios.get(`http://localhost:8080/api/v1/devices/${id}`).then((res) => {
      setData(res.data);
    });
  }

  const [name, setName] = useState(initialState.name);
  const [description, setDescription] = useState(initialState.description);
  const [serialNumber, setSerialNumber] = useState(initialState.serial);
  const [status, setStatus] = useState(initialState.status);

  const handleNameOnChange = (e) => {
    setName(e.target.value);
  };
  const handleDescriptionOnChange = (e) => {
    setDescription(e.target.value);
  };

  const handleSerialNumberOnChange = (e) => {
    setSerialNumber(e.target.value);
  };

  const handleStatusOnChange = (e) => {
    if (e.target.checked) {
      setStatus(true);
    }
  };

  const handleSubmitButton = () => {
    let checkName = name === "" ? data.deviceName : name;
    let checkDescription = description === "" ? data.deviceDescription : description;
    let checkSerialNumber = serialNumber === "" ? data.deviceSerialNumber : serialNumber;
    let checkStatus = status === "" ? data.deviceStatus : status;
 
    let updateDevice = {
      "deviceName": checkName,
      "deviceDescription": checkDescription,
      "deviceSerialNumber": checkSerialNumber,
      "deviceStatus": checkStatus 
    }

    axios
      .put(`http://localhost:8080/api/v1/devices/${id}`, updateDevice)
      .then((res) => {
        loadData();
        console.log(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <HeaderComponent />
      <div className="new-device-main-container">
        <div className="new-device-container">
          <div className="new-device-container-header">
            <h4>Update Existing Device</h4>
          </div>
          <div className="new-device-container-form">
            <Form>
              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="4">
                  Name
                </Form.Label>
                <Col sm="8">
                  <Form.Control
                    onChange={handleNameOnChange}
                    defaultValue={data.deviceName}
                    type="text"
                    placeholder="Enter Name"
                  />
                </Col>
              </Form.Group>

              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="4">
                  Description
                </Form.Label>
                <Col sm="8">
                  <Form.Control
                    onChange={handleDescriptionOnChange}
                    defaultValue={data.deviceDescription}
                    type="text"
                    placeholder="Enter Description"
                  />
                </Col>
              </Form.Group>

              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="4">
                  SerialNumber
                </Form.Label>
                <Col sm="8">
                  <Form.Control
                    onChange={handleSerialNumberOnChange}
                    defaultValue={data.deviceSerialNumber}
                    type="text"
                    placeholder="Enter Serial Number"
                  />
                </Col>
              </Form.Group>

              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="4">
                  Status
                </Form.Label>
                <Col sm="8">
                  <Form.Check
                    onChange={handleStatusOnChange}
                    defaultValue={data.deviceStatus ? true : false}
                    type="checkbox"
                    label="Active"
                  />
                </Col>
              </Form.Group>
            </Form>
          </div>
          <div className="new-device-container-button">
            <Button
              onClick={handleSubmitButton}
              className="button-add"
              variant="primary"
            >
              Submit
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UpdateDeviceComponent;
