import {useState, useEffect} from "react";
import HeaderComponent from "../view/HeaderComponent";
import "../../stylesheets/TenantForm.css";
import { Form, Row, Col, Button } from "react-bootstrap";
import { Redirect, useParams } from "react-router-dom";
import axios from "axios";

const UpdateTenantComponent = () => {

  const { id } = useParams();
  const [data, setData] = useState([]);

  useEffect(() => {
    axios.get(`http://localhost:8080/api/v1/tenants/${id}`).then((res) => {
      setData(res.data);
    });
  }, [id]);

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [domain, setDomain] = useState('');
  const [address, setAddress] = useState('');
  const [username, setUsername] = useState('');
  const [status, setStatus] = useState('');

  const handleNameOnChange = (e) => {
    setName(e.target.value);
    console.log(e.target.value)
  };
  const handleDescriptionOnChange = (e) => {
    console.log(e.target.value)
    setDescription(e.target.value);
  };

  const handleDomainOnChange = (e) => {
    console.log(e.target.value)
    setDomain(e.target.value);
  };

  const handleAddressOnChange = (e) => {
    console.log(e.target.value)
    setAddress(e.target.value);
  };

  const handleUserNameOnChange = (e) => {
    console.log(e.target.value)
    setUsername(e.target.value);
  };

  const handleStatusOnChange = (e) => {
    console.log(e.target.checked)
    if (e.target.checked) {
    setStatus(true);
    }
  };

  const handleSubmitButton = () => {
    console.log(data);

    let checkName = name === "" ? data.tenantName : name;
    let checkDescription = description === "" ? data.tenantDescription : description
    let checkUserName = username === "" ? data.tenantUsername : username;
    let checkDomain = domain === "" ? data.tenantDomain : domain;
    let checkAddress = address === "" ? data.tenantAddress : address;
    let checkStatus = status === "" ? data.tenantStatus : status;

    console.log(checkName);

    var newDataObject = {
      "tenantName": checkName,
      "tenantDescription": checkDescription,
      "tenantUsername": checkUserName,
      "tenantStatus": checkStatus,
      "tenantDomain": checkDomain,
      "tenantAddress": checkAddress,
      "tenantDevices": data.tenantDevices,
    };

    axios
      .put(`http://localhost:8080/api/v1/tenants/${id}`, newDataObject)
      .then((res) => {
        console.log(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
    window.alert("Updated the Edited information");
  };

  return (
    <div>
      <HeaderComponent />
      <div className="new-tenant-main-container">
        <div className="new-tenant-container">
          <div className="new-tenant-container-header">
            <h4>Add New Tenant</h4>
          </div>
          <div className="new-tenant-container-form">
            <Form>
              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="4">
                  Name
                </Form.Label>
                <Col sm="8">
                  <Form.Control defaultValue={data.tenantName} onChange={handleNameOnChange} type="text" placeholder="Enter Name" />
                </Col>
              </Form.Group>

              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="4">
                  Description
                </Form.Label>
                <Col sm="8">
                  <Form.Control defaultValue={data.tenantDescription} onChange={handleDescriptionOnChange} type="text" placeholder="Enter Description" />
                </Col>
              </Form.Group>

              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="4">
                  Username
                </Form.Label>
                <Col sm="8">
                  <Form.Control defaultValue={data.tenantUsername} onChange={handleUserNameOnChange} type="text" placeholder="Enter Username" />
                </Col>
              </Form.Group>

              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="4">
                  Domain
                </Form.Label>
                <Col sm="8">
                  <Form.Control defaultValue={data.tenantDomain} onChange={handleDomainOnChange} type="text" placeholder="Enter Domain" />
                </Col>
              </Form.Group>

              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="4">
                  Address
                </Form.Label>
                <Col sm="8">
                  <Form.Control defaultValue={data.tenantAddress} onChange={handleAddressOnChange} type="text" placeholder="Enter Address" />
                </Col>
              </Form.Group>

              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="4">
                  Status
                </Form.Label>
                <Col sm="8">
                  <Form.Check defaultValue={data.tenantStatus} onChange={handleStatusOnChange} type="checkbox" label="Active" />
                </Col>
              </Form.Group>
            </Form>
          </div>
          <div className="new-tenant-container-button">
            <Button onClick={handleSubmitButton} className="button-add" variant="primary">
              Submit
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UpdateTenantComponent;
