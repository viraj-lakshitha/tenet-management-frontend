import { useState } from "react";
import { Redirect } from "react-router-dom";
import HeaderComponent from "../view/HeaderComponent";
import axios from "axios";
import { Form, Row, Col, Button } from "react-bootstrap";
import "../../stylesheets/TenantForm.css";

const initialState = {
  name : "",
  description : "",
  domain : "",
  address : "",
  username : "",
  status : false,
}

const AddTenantComponent = () => {
  const [name, setName] = useState(initialState.name);
  const [description, setDescription] = useState(initialState.description);
  const [domain, setDomain] = useState(initialState.domain);
  const [address, setAddress] = useState(initialState.address);
  const [username, setUsername] = useState(initialState.username);
  const [status, setStatus] = useState(initialState.status);

  const handleNameOnChange = (e) => {
    setName(e.target.value);
    console.log(e.target.value)
  };
  const handleDescriptionOnChange = (e) => {
    console.log(e.target.value)
    setDescription(e.target.value);
  };

  const handleDomainOnChange = (e) => {
    console.log(e.target.value)
    setDomain(e.target.value);
  };

  const handleAddressOnChange = (e) => {
    console.log(e.target.value)
    setAddress(e.target.value);
  };

  const handleUserNameOnChange = (e) => {
    console.log(e.target.value)
    setUsername(e.target.value);
  };

  const handleStatusOnChange = (e) => {
    console.log(e.target.checked)
    if (e.target.checked) {
    setStatus(true);
    }
  };

  const handleSubmitButton = () => {
    let newTenant = {
      "tenantName": name,
      "tenantDescription": description,
      "tenantDomain": domain,
      "tenantAddress": address,
      "tenantUsername": username,
      "tenantStatus": status,
      "tenantDevices": [],
    };

    axios.post(`http://localhost:8080/api/v1/tenants`, newTenant )
      .then(res => {
        console.log(res);
        return <Redirect to='/'/>;
      })
  };

  return (
    <div>
      <HeaderComponent />
      <div className="new-tenant-main-container">
        <div className="new-tenant-container">
          <div className="new-tenant-container-header">
            <h4>Add New Tenant</h4>
          </div>
          <div className="new-tenant-container-form">
            <Form>
              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="4">
                  Name
                </Form.Label>
                <Col sm="8">
                  <Form.Control onChange={handleNameOnChange} type="text" placeholder="Enter Name" />
                </Col>
              </Form.Group>

              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="4">
                  Description
                </Form.Label>
                <Col sm="8">
                  <Form.Control onChange={handleDescriptionOnChange} type="text" placeholder="Enter Description" />
                </Col>
              </Form.Group>

              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="4">
                  Username
                </Form.Label>
                <Col sm="8">
                  <Form.Control onChange={handleUserNameOnChange} type="text" placeholder="Enter Username" />
                </Col>
              </Form.Group>

              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="4">
                  Domain
                </Form.Label>
                <Col sm="8">
                  <Form.Control onChange={handleDomainOnChange} type="text" placeholder="Enter Domain" />
                </Col>
              </Form.Group>

              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="4">
                  Address
                </Form.Label>
                <Col sm="8">
                  <Form.Control onChange={handleAddressOnChange} type="text" placeholder="Enter Address" />
                </Col>
              </Form.Group>

              <Form.Group
                as={Row}
                className="mb-3"
                controlId="formPlaintextPassword"
              >
                <Form.Label column sm="4">
                  Status
                </Form.Label>
                <Col sm="8">
                  <Form.Check onChange={handleStatusOnChange} type="checkbox" label="Active" />
                </Col>
              </Form.Group>
            </Form>
          </div>
          <div className="new-tenant-container-button">
            <Button className="button-add" onClick={handleSubmitButton} variant="primary">
              Submit
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddTenantComponent;
