import {useState} from "react";
import "../../stylesheets/DeviceForm.css";
import { Form, Row, Col, Button } from "react-bootstrap";
import HeaderComponent from "../view/HeaderComponent";
import axios from "axios";
import { useParams } from "react-router-dom";

const initialState = {
  name : "",
  description : "",
  serial : "",
  status : false,
}

const AddDeviceComponent = () => {

  const { id } = useParams();

  const [name, setName] = useState(initialState.name);
  const [description, setDescription] = useState(initialState.description);
  const [serialNumber, setSerialNumber] = useState(initialState.serial);
  const [status, setStatus] = useState(initialState.status);

  const handleNameOnChange = (e) => {
    setName(e.target.value);
    console.log(e.target.value)
  };
  const handleDescriptionOnChange = (e) => {
    console.log(e.target.value)
    setDescription(e.target.value);
  };

  const handleSerialNumberOnChange = (e) => {
    console.log(e.target.value)
    setSerialNumber(e.target.value);
  };

  const handleStatusOnChange = (e) => {
    console.log(e.target.checked)
    if (e.target.checked) {
    setStatus(true);
    }
  };

  const handleSubmitButton = () => {
    if (
      name === initialState.name ||
      description === initialState.description ||
      serialNumber === initialState.serial
    ) {
      console.log("Please Enter Values");
    } else {
      var newDataObject = {
        "deviceName": name,
        "deviceDescription": description,
        "deviceSerialNumber": serialNumber,
        "deviceStatus": status,
      };
      axios
        .get(`http://localhost:8080/api/v1/tenants/${id}`)
        .then((res) => {
          const data = res.data;
          data.tenantDevices.push(newDataObject);
          axios
            .put(`http://localhost:8080/api/v1/tenants/${id}`, data)
            .then((res) => {
              console.log(res.data)
            })
            .catch((error) => {
              console.log(error);
            });
        });
    }
  };

  return (
    <div>
        <HeaderComponent />
        <div className="new-device-main-container">
      <div className="new-device-container">
        <div className="new-device-container-header">
          <h4>Add New Device</h4>
        </div>
        <div className="new-device-container-form">
          <Form>
            <Form.Group
              as={Row}
              className="mb-3"
              controlId="formPlaintextPassword"
            >
              <Form.Label column sm="4">
                Name
              </Form.Label>
              <Col sm="8">
                <Form.Control onChange={handleNameOnChange} type="text" placeholder="Enter Name" />
              </Col>
            </Form.Group>

            <Form.Group
              as={Row}
              className="mb-3"
              controlId="formPlaintextPassword"
            >
              <Form.Label column sm="4">
                Description
              </Form.Label>
              <Col sm="8">
                <Form.Control onChange={handleDescriptionOnChange} type="text" placeholder="Enter Description" />
              </Col>
            </Form.Group>

            <Form.Group
              as={Row}
              className="mb-3"
              controlId="formPlaintextPassword"
            >
              <Form.Label column sm="4">
                SerialNumber
              </Form.Label>
              <Col sm="8">
                <Form.Control onChange={handleSerialNumberOnChange} type="text" placeholder="Enter Username" />
              </Col>
            </Form.Group>

            <Form.Group
              as={Row}
              className="mb-3"
              controlId="formPlaintextPassword"
            >
              <Form.Label column sm="4">
                Status
              </Form.Label>
              <Col sm="8">
                <Form.Check onChange={handleStatusOnChange} type="checkbox" label="Active" />
              </Col>
            </Form.Group>
          </Form>
        </div>
        <div className="new-device-container-button">
          <Button onClick={handleSubmitButton} className="button-add" variant="primary">
            Submit
          </Button>
        </div>
      </div>
    </div>
    </div>
  );
};

export default AddDeviceComponent;
